package com.words.wordAPI.controller;

import com.words.wordAPI.adapter.WordAdapterImpl;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@Tag(name = "Word API", description = "API for managing words")
@RequestMapping("/api/words")
public class WordController {
    private WordAdapterImpl adapter;

    @Operation(summary = "Insert a new word", description = "Adds a new word to the word list")
    @PostMapping("")
    private ResponseEntity<String> addWord(@RequestParam String word) {
        if (StringUtils.hasLength(word)) {
            adapter.addWord(word);
        } else {
            return ResponseEntity.status(404).body("word empty");
        }
        return ResponseEntity.ok("word added");
    }

    @Operation(summary = "Delete a word", description = "Removes a word from the list")
    @DeleteMapping("/{word}")
    private ResponseEntity<String> removeWord(@PathVariable String word) {
        if (adapter.removeWord(word)) {
            return ResponseEntity.ok("word removed");
        } else {
            return ResponseEntity.status(404).body("Word not found");
        }
    }

    @Operation(summary = "Update a word", description = "Updates an existing word in the list")
    @PutMapping("/{oldWord}")
    public ResponseEntity<String> updateWord(@PathVariable String oldWord, @RequestParam String newWord) {
        if (adapter.updateWord(oldWord, newWord)) {
            return ResponseEntity.ok("Word updated successfully");
        } else {
            return ResponseEntity.status(404).body("Word not found");
        }
    }

    @Operation(summary = "Find a word", description = "Finds a word in the list")
    @GetMapping("/{word}")
    public ResponseEntity<String> findWord(@PathVariable String word) {
        String foundWord = adapter.findWord(word);
        if (foundWord != null) {
            return ResponseEntity.ok("Word found: " + foundWord);
        } else {
            return ResponseEntity.status(404).body("Word not found");
        }
    }

    @Operation(summary = "Get all words", description = "Retrieves all words in the list")
    @GetMapping("/")
    public ResponseEntity<List<String>> getAllWords() {
        return ResponseEntity.ok(adapter.getAllWords());
    }

    @Operation(summary = "Get all palindromes", description = "Finds all palindromic words in the list")
    @GetMapping("/palindromes")
    public ResponseEntity<List<String>> getPalindromes() {
        return ResponseEntity.ok(adapter.getPalindromes());
    }

    public void setAdapter(WordAdapterImpl adapter) {
        this.adapter = adapter;
    }
}
