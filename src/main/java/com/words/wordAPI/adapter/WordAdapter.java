package com.words.wordAPI.adapter;

import java.util.List;

public interface WordAdapter {
    void addWord(String word);
    boolean removeWord(String word);
    boolean updateWord(String oldWord, String newWord);
    String findWord(String word);
    List<String> getAllWords();
    List<String> getPalindromes();
}
