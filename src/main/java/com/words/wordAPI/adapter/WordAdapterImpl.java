package com.words.wordAPI.adapter;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class WordAdapterImpl implements WordAdapter {
    private final List<String> words = new ArrayList<>();

    public void addWord(String word) {
        words.add(word);
    }

    public boolean removeWord(String word) {
        return words.remove(word);
    }

    public boolean updateWord(String oldWord, String newWord) {
        int index = words.indexOf(oldWord);
        if (index != -1) {
            words.set(index, newWord);
            return true;
        }
        return false;
    }

    public String findWord(String word) {
        return words.contains(word) ? word : null;
    }

    public List<String> getAllWords() {
        return new ArrayList<>(words);
    }

    public List<String> getPalindromes() {
        return words.stream()
                .filter(Objects::nonNull)
                .filter(word -> word.contentEquals(new StringBuilder(word).reverse()))
                .collect(Collectors.toList());
    }
}