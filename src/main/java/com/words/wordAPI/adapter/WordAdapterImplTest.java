package com.words.wordAPI.adapter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class WordAdapterImplTest {
    private WordAdapterImpl wordAdapter;

    @BeforeEach
    void setUp() {
        wordAdapter = new WordAdapterImpl();
    }

    @Test
    void testAddWord() {
        wordAdapter.addWord("test");
        assertEquals(1, wordAdapter.getAllWords().size());
        assertTrue(wordAdapter.getAllWords().contains("test"));
    }

    @Test
    void testRemoveWord() {
        wordAdapter.addWord("test");
        assertTrue(wordAdapter.removeWord("test"));
        assertFalse(wordAdapter.getAllWords().contains("test"));
    }

    @Test
    void testUpdateWord() {
        wordAdapter.addWord("old");
        assertTrue(wordAdapter.updateWord("old", "new"));
        assertFalse(wordAdapter.getAllWords().contains("old"));
        assertTrue(wordAdapter.getAllWords().contains("new"));
    }

    @Test
    void testFindWord() {
        wordAdapter.addWord("search");
        assertEquals("search", wordAdapter.findWord("search"));
    }

    @Test
    void testGetAllWords() {
        wordAdapter.addWord("first");
        wordAdapter.addWord("second");
        List<String> words = wordAdapter.getAllWords();
        assertEquals(2, words.size());
        assertTrue(words.contains("first"));
        assertTrue(words.contains("second"));
    }

    @Test
    void testGetPalindromes() {
        wordAdapter.addWord("level");
        wordAdapter.addWord("word");
        List<String> palindromes = wordAdapter.getPalindromes();
        assertEquals(1, palindromes.size());
        assertTrue(palindromes.contains("level"));
    }
}
